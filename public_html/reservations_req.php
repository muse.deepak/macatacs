
<?php

// First check to see if the for has been submitted
if (isset($_POST['submitted'])) {

  $errors = array(); // Initialize error array.

  // Check for a name
  if (empty($_POST['fname'])) {
    $errors[] = 'You forgot to enter your Name.';
  }

  if (empty($_POST['phone'])) {
    $errors[] = 'You forgot to enter your Phone number.';
  }

  // Check for a valid email address
  if (!preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $_POST['email'])) {
    $errors[] = 'You need to enter a valid email address.';
  }

  if (empty($_POST['hear_about'])) {
    $errors[] = 'You forgot to enter How Did Hear About Us.';
  }

  if (empty($_POST['dates'])) {
    $errors[] = 'You forgot to enter Interested Dates Of Charter.';
  }

  if (!empty($_POST['human'])) {
    $errors[] = 'Sorry cannot validate you.';
  }

  if (empty($errors)) { // If everything is OK.

    //Process multiple selection    
    //$aCountries = $_POST['formCountries'];

  	//for($i=0; $i < 5; $i++)
      //{
       // $selected_v += $aCountries[$i] . " ";
  	  //echo($aCountries[$i] . " ");
      //}

    // Let's send an email
    // Let's first send some email to the admin
    $mailTo = "macatac.sportfishing@gmail.com";
    //$mailTo = "annatsiz@gmail.com";
    $senderName .= $_POST['fname'];
    $senderMail = $_POST['email'];
    $break .= "<br/>";
    $eol .= "\r\n";
    $sol .= "\n";
    $headers .= 'To: '.$mailTo.' <'.$mailTo.'>'.$eol;
    $headers .= 'From: '.$senderName.' <'.$senderMail.'>'.$eol;
    $headers .= 'Date: '.date("r").$eol;
    $headers .= 'Sender-IP: '.$_SERVER["REMOTE_ADDR"].$eol;
    $headers .= 'X-Mailser: MCT Adv.PHP Mailer 1.0'.$eol;
    $headers .= 'MIME-Version: 1.0'.$eol;
    //$headers .= 'Content-Type: text/html; charset="windows-1251"\r\n';
    $headers .= 'Content-Type: text/html; charset="iso-8859-1"'.$eol;
    $subject = 'MACATAC Reservations Form';

    $msg .= '<font face=arial size=2>';
    $msg .= 'You have recieved a reservation request from MACATAC'.$break.$break;
    $msg .= '<strong>Name: </strong> '.$_POST['fname'].$break;
    $msg .= '<strong>Preferred Phone #: </strong> '.$_POST['phone'].$break;
    $msg .= '<strong>E-Mail: </strong> '.$_POST['email'].$break;
    $msg .= '<strong>How Did Hear About Us *: </strong> '.$_POST['hear_about'].$break;
    $msg .= '<strong>Interested Dates Of Charter *: </strong> '.$_POST['dates'].$break;;
    $msg .= '<strong>Type Of Charter: </strong> '.$_POST['charter_type'].$break;
    $msg .= '<strong>Question/Message: </strong> '.$_POST['message'].$break;
    $msg .= $break;
    $msg .= '</font>';

    // Mail it
    mail($mailTo, $subject, $msg, $headers);

    echo '<p if="mainhead">Thank you!</p>
    <p class="main_txt">Thanks for submitting your reservation. Someone will get back to you as soon as possible.</p>';

  } else {
    echo '<p id="mainhead">Error!</p>
    <p class="main_txt">The following error(s) occured.<br/>';
    foreach ($errors as $errorMSG) { // Print each error.
      echo " - $errorMSG<br/>\n";
    } // End of Errors

    echo 'Please go <a href="Javascript:history.go(-1)">back</a> and try again.</p>';

  } // End of if (empty($errors)) IF Statement

} else { // Display the form.

  echo'
  <p>Please fill in the form below to make a reservation.</p>
  <form method="post">
  <table border="0" cellspacing="4" cellpadding="4" width="600px">
          <tr>
            <td width="319" height="37"><label for="customerName">Name *: </label></td>
            <td width="253" align="left"><input class="contactBox" type="text" size="30" name="fname" id="fname" value="'.$_POST['fname'].'" /></td>
          </tr>
  		<tr>
            <td width="319" height="37"><label for="customerName">Preferred Phone # *: </label></td>
            <td width="253" align="left"><input class="contactBox" type="text" size="30" name="phone" id="phone" value="'.$_POST['phone'].'" /></td>
          </tr>
            <td height="37"><label for="email">Email *:</label></td>
            <td align="left"><input class="contactBox" type="text" size="30" name="email" id="email" value="'.$_POST['email'].'" /></td>
          </tr>
  		<tr>
            <td width="319" height="37"><label for="customerName">How Did Hear About Us *: </label></td>
           <td width="253" align="left">
  		  <select name="hear_about" style="font-family:Geneva, sans-serif; font-size:14px; color:#5d6469";>
  		  	<option value="0">--Select--</option>
  			<option value="Current customer">Current customer</option>
        <option value="Past customer">Past customer</option>
        <option value="Referral">Referral</option>
        <option value="Web site">Web site</option>
        <option value="Article">Article</option>
        <option value="AedisWebdesign.com">AedisWebdesign.com</option>
        <option value="Search Engine">Search Engine</option>
        <option value="Directory">Directory</option>
        <option value="Other">Other</option>
  			</select>
  		 </td>
          </tr>
  		<tr>
            <td width="319" height="37"><label for="customerName">Interested Dates Of Charter *: </label></td>
            <td width="253" align="left"><input class="contactBox" type="text" size="30" name="dates" id="dates" value="'.$_POST['dates'].'" /></td>
          </tr>
  		<tr>
            <td width="319" height="37"><label for="customerName">Type Of Charter: </label></td>
            <td width="253" align="left"><input class="contactBox" type="text" size="30" name="charter_type" id="charter_type" value="'.$_POST['charter_type'].'" /></td>
          </tr>
          <tr>
            <td height="112"><label for="message">Question or Comment: </label></td>
            <td align="left"><textarea name="message" cols="33" rows="5" id="message">'.$_POST['$message'].'</textarea>
  		  <input type="text" style="display:none; visibility:hidden" name="human" id="human" /></td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="right"></td>
            <td align="center" ><input name="submit" type="submit" value="Submit" tabindex="5" /><input type="hidden" name="submitted" value="TRUE" /> &nbsp; <input name="reset" type="reset" value="Reset" tabindex="6" /></td>
          </tr>
          <tr>
            <td colspan="2" align="center"></td>
          </tr>
        </table>
  </p>';

} // End of submitted IF-ELSE statement

?> 

