<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/index.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>MAC-ATAC Sportfishing Charters | Reservations</title>
<meta name="description" content="Here you can see a small sample of the fish caught by Mac-Atac's customers. Family fishing, serious sports fishing, recreational fishing, and small group fishing around Buzzard's Bay, Elizabeth Island, Cuttyhunk, Martha's Vineyard, Cape surrounding islands.">
<meta name="keyword" content="Mac-Atac Buzzard's Bay Fishing, Elizabeth Island, Cuttyhunk Fishing, Martha's Vineyard fishing, Cuttyhunk sports fishing guide, Martha's Vineyard sports fishing guide Mac-Atac, trophy stripers caught Buzzard's Bay, trophy stripers Elizabeth Island, trophy stripers Cuttyhunk, trophy stripers Martha's Vineyard and surrounding cape Islands, jumbo blues, blue fish, mackerel, fluke, scup, sea, bass, tautog, striped bass, offshore, tuna, shark, mahi-mahi, fishing sport, Mac-Atac .">



<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css" />
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-30561693-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript">
function show_year () {
	var theDate=new Date();
	document.getElementById("curr_year").innerHTML=theDate.getFullYear();
}
</script>
</head>

<body onload="show_year()">
<table width="858px" cellpadding="0" cellspacing="0" border="0" align="center">
<tr>
<td class="banner" valign="top"><div style="margin:12px">
<ul id="MenuBar1" class="MenuBarHorizontal">
  <li><a href="index.html">Home</a>    </li>
  <li><a href="our_trips.html" class="MenuBarItemSubmenu">Our Trips</a>
    <ul>
      <li><a href="our_trips.html#half_day_trips">Half Day Trips </a></li>
      <li><a href="our_trips.html#extended_day_trips">Extended Day Trips</a></li>
      <li><a href="our_trips.html#off_shore_charters">Off-shore charters </a></li>
      <li><a href="our_trips.html">Full Day/Night Charter </a></li>
      <li><a href="our_trips.html#family_fun_day">Family Fun Day</a></li>
      <li><a href="reservations.php">Reservations</a></li>
    </ul>
  </li>
   <li><a class="MenuBarItemSubmenu" href="#">Captains</a>
    <ul>
      <li><a href="capatain_todd.html">Captain Todd</a>        </li>
      <li><a href="capatain_casey.html">Captain Casey</a></li>
      <li><a href="why_choose_us.html">Why Choose Us?</a></li>
    </ul>
  </li>
  <li><a href="" class="MenuBarItemSubmenu">Details</a>
    <ul>
      <li><a href="what_we_supply.html">What We Supply</a></li>
      <li><a href="what_to_bring.html">What To Bring</a></li>
      <li><a href="safety_rules.html">Safety Rules</a></li>
      <li><a href="cancellations_and_no_shows.html">Cancellations &amp; No Shows</a></li>
      <li><a href="lodging_and_dining.html">Lodging &amp; Dining</a></li>
    </ul>
  </li>
  <li><a href="directions.html">Directions</a>    </li>
</ul>

</div>
<br/>
<a href="mailto:macatac.sportfishing@gmail.com"><img style="margin-left:30px; margin-top:125px;" src="images/contact_email.gif" width="350" height="18" alt="Contact email" border="0" /></a>

</td>
</tr>
<tr>
<td><div style="margin:20px"><!-- InstanceBeginEditable name="BodyContent" -->
<a title="Print Page" href="PDFprint/__www.macatacsportfishing.com_reservations.pdf" target="_blank"><img src="images/icon_print.png" alt="Print Page" width="16" height="16" border="0" align="right" /></a>
<h2>Reservations</h2>
 <?php include "reservations_req.php";?>
<!--<table border="0" cellspacing="4" cellpadding="4" width="600px">
  <tr>
    <td width="319"><p><strong>Name *:</strong></p></td>
    <td width="253"><p>
      <input type="text" size="40" name="name" />
    </p></td>
  </tr>
  <tr>
    <td width="319"><p><strong>Preferred Phone # *</strong>:</p></td>
    <td width="253"><p>
      <input type="text" size="40" name="phone" />
    </p></td>
  </tr>
  <tr>
    <td width="319"><p><strong>Your Email Address:</strong></p></td>
    <td width="253"><p>
      <input type="text" size="40" name="email" />
    </p></td>
  </tr>
  <tr>
    <td width="319"><p><strong>How Did Hear About Us *</strong>:</p></td>
    <td width="253"><p>
      <select>
      <option>--Select--</option>
      <option>Current customer</option>
      <option>Past customer</option>
      <option>Referral</option>
      <option>Web site</option>
      <option>Article</option>
      <option>AedisWebdesign.com</option>
      <option>Search Engine</option>
      <option>Directory</option>
      <option>Other</option>
      </select>
    </p></td>
  </tr>
  <tr>
    <td width="319"><p><strong>Interested Dates Of&nbsp;Charter *:</strong></p></td>
    <td width="253"><p>
      <input type="text" size="40" name="dates" />
    </p></td>
  </tr>
  <tr>
    <td width="319"><p><strong>Type Of Charter:</strong></p></td>
    <td width="253"><input type="text" size="40" name="dates" /></td>
  </tr>
  <tr>
    <td width="319"><p><strong>Question or Comment:</strong></p></td>
    <td width="253"><p>
      <textarea rows="10" cols="40" name="message"></textarea>
    </p></td>
  </tr>
  <tr>
    <td colspan="2"></td>
  </tr>
  <tr>
    <td colspan="2" align="right"><input name="submit" type="button" value="Submit" /> &nbsp; <input name="reset" type="button" value="Reset" /></td>
  </tr>
</table>
  <table align="center" width="100%">
  <tr>
  <td><img src="images/Gibbs Logo.jpg" width="191" height="75" alt="Gibbs" /></td>
  <td><h4>If catching fish: especially Striped Bass, Bluefish and offshore species  is what you want, <br />
    then this is the place to call and book your next fishing trip</h4></td>
  <td><img src="images/Daiwa_logo.jpg" width="166" height="78" alt="Daiwa" /></td>
  </tr>
  </table>-->
<!-- InstanceEndEditable --></div></td>
</tr>
<tr>
<td colspan="2" class="copyright">

    <a target="_blank" href="https://www.facebook.com/macatac.sportfishing"><img align="absmiddle" border="0" src="images/facebook.jpg" width="25" height="24" alt="Visit us on facebook" /></a>
    <br/><br/><span style="color:#090;"><strong>Phone: 508-243-8559</strong></span>
    <br/>
Email: <a class="copyright" href="mailto:macatac.sportfishing@gmail.com">macatac.sportfishing@gmail.com</a><br/>
<a class="copyright" href="index.html">Home</a> |
<!-- <a class="copyright" href="boat_and_equipment.html">Our Boat</a> | -->
<a class="copyright" href="pictures.html">Pictures</a> |
<a class="copyright" href="our_trips.html">Our Trips</a> |
<a class="copyright" href="in_the_news.html">In the News</a> |
<a class="copyright" href="articles.html">Articles</a> |
<a class="copyright" href="directions.html">Contact &amp; Directions</a><br/>
<span id="curr_year"></span> Copyright, MacAtac Sportfishing. All Rights Reserved. Fairhaven, MA  <br/>
Design &amp; SEO by <a target="_blank" class="copyright" href="http://www.aediswebdesign.com">AedisWebDesign.com</a> | "Affordable & Efficient Design & Internet Services"

</td>
</tr>
</table>
<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</body>
<!-- InstanceEnd --></html>
